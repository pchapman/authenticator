module gitlab.com/pchapman/authenticator

replace gitlab.com/pchapman/gooauth => ../gooauth

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/zserge/webview v0.0.0-20191103184548-1a9ebffc2601
	gitlab.com/pchapman/gooauth v1.0.1
	gopkg.in/yaml.v2 v2.2.1
)

go 1.13
