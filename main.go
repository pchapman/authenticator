package main

import (
	"flag"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/zserge/webview"
	"gitlab.com/pchapman/authenticator/tokens"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"text/template"
	"time"
)

// Initialization parameters to come from command line along with reasonable defaults.
var (
	configFilePath = flag.String("c", "", "The configuration file to load environments from.")
	debug          = flag.Bool("d", false, "Debug the application, showing additional info.")
	envName        = flag.String("p", "", "The profile to authenticate against.")
	//	openBrowser = flag.Bool("b", true, "Disables the normal practice of opening the default browser, if required.  Instead a URL is printed to command line.")
	skipCache    = flag.Bool("s", false, "Skips storing tokens in cache")
	printVersion = flag.Bool("v", false, "Print version info and exit.")
)

const port = 9999

var server *http.Server

// This value is set in build:
// go build -ldflags "-X main.version=1.0.0"
var version string

// The key for the token in the cache
var tokenKey string
var tokenPath string

func main() {
	flag.Parse()

	if *printVersion {
		fmt.Printf("OAuth Authenticator: version %s\n", version)
		os.Exit(0)
	}

	if !*debug {
		log.SetFlags(0)
		log.SetOutput(ioutil.Discard)
	}

	usr, err := user.Current()
	if err != nil {
		log.Fatalf("Failed to locate the user's home directory: %s\n", err)
	}

	// Authenticator data dir
	authenticatorPath := fmt.Sprintf("%s%s.authenticator", usr.HomeDir, string(os.PathSeparator))
	stat, err := os.Stat(authenticatorPath)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(authenticatorPath, 0700)
			if err != nil {
				log.Fatalf("Unable to create the directory %s", authenticatorPath)
			}
		} else {
			log.Fatalf("Unable to read status of directory %s", authenticatorPath)
		}
	} else {
		if !stat.IsDir() {
			log.Fatalf("The path %s is not a directory", authenticatorPath)
		}
	}

	// Load Environments
	if "" == *configFilePath {
		s := fmt.Sprintf("%s%sprofiles.yaml", authenticatorPath, string(os.PathSeparator))
		configFilePath = &s
	}
	log.Printf("Loading environments from %s", *configFilePath)
	profiles, err := LoadProfiles(*configFilePath)
	if err != nil {
		log.Fatalf("Failed to load environments from file %s: %s\n", *configFilePath, err)
	}

	err = loadSigningTokens(profiles)
	if err != nil {
		log.Fatalf("Unable to load environments: %s", err)
	}

	profile := profiles.Find(*envName)
	if nil == profile {
		log.Fatalf("Unable to locate environment %s in file %s\n", *envName, *configFilePath)
	}
	log.Printf("Selected environment %s, tenant %s", profile.Name, profile.AuthConfig.Domain)

	var ok bool
	var token string
	var tokensSlice tokens.StoredTokens

	if !*skipCache {
		// Load any saved tokens
		tokenKey = fmt.Sprintf("%s::%s", profile.AuthConfig.Domain, *envName)
		tokenPath = fmt.Sprintf("%s%s.authenticator%s.tokens.yaml", usr.HomeDir, string(os.PathSeparator), string(os.PathSeparator))
		log.Printf("The tokenPath is %s", tokenPath)
		tokensSlice, err = tokens.LoadTokens(tokenPath)
		if err != nil {
			log.Fatalf("Unable to load stored tokens: %s", err)
		}

		// See if we have a valid token for the selected environments
		token, ok = tokensSlice[tokenKey]
		if ok {
			// We have a match by name.  Now test to see if it is still valid.
			t, err := jwt.Parse(token, signingKeyForToken)
			if err == nil && t.Valid {
				log.Printf("Existing token found for key %s is valid", tokenKey)
				fmt.Println(token)
				os.Exit(0)
			} else {
				log.Printf("Existing token found for key %s is not valid, continuing.", tokenKey)
			}
		}
	}

	if profile.IsServerToServer() {
		// If it's a server-to-server configuration, we can simply request the token.
		token, err = profile.ObtainAccessToken()
		if err != nil {
			log.Fatalf("Unable to obtain a token: %s", err)
		} else {
			if !*skipCache {
				tokensSlice[tokenKey] = token
				err = tokensSlice.SaveTokens(tokenPath)
				if err != nil {
					log.Fatal("Unable to store token: %s", err)
				}
			}
			fmt.Println(token)
			os.Exit(0)
		}
		//} else if profile.IsUserAuthenticated() {
		//	if profile.GetAuthenticationProviderType() == gooauth.AP_Auth0 {
		//		// If we get this far, no (valid) token.  Start the http server and have the user log in.
		//		authenticateUser(profile, tokenKey, tokenPath)
		//	} else {
		//		log.Fatalf("The authentication provider of environment %s is not supported.  Only Auth0.", profile.Name)
		//	}
	} else {
		log.Fatalf("Authentication for the environment %s is not possible.", profile.Name)
	}
}

func authenticateUser(profile *Profile, tokenKey string, tokenPath string) {
	finished := make(chan *string)
	authServer = *newAuthServer(profile, tokenKey, tokenPath, finished)
	go authServer.startServer()
	url := fmt.Sprintf("http://localhost:%d/", port)
	//	if *openBrowser {
	webview.Open("Authenticator", url, 800, 600, true)
	//	} else {
	//		fmt.Printf("In order to complete authentication, open your browser and navigate to %s\n", url)
	//	}

	authServer.token = <-authServer.finished
	log.Printf("Authentication completed")

	//	fmt.Println(authServer.token)
}

type authServerStruct struct {
	ClientId     string
	ServiceAppId string
	TenantId     string

	finished   chan *string
	token      *string
	tokenKey   string
	tokenPath  string
	httpServer *http.Server
}

func newAuthServer(profile *Profile, tokenKey string, tokenPath string, finished chan *string) *authServerStruct {
	return &authServerStruct{
		ClientId:     profile.AuthConfig.ClientId,
		ServiceAppId: profile.ServiceAppId,
		TenantId:     "", //TODO:

		finished:  finished,
		tokenKey:  tokenKey,
		tokenPath: tokenPath,
	}
}

var authServer authServerStruct

func (*authServerStruct) startServer() {
	// Listen for requests
	http.HandleFunc("/report", reportToken)
	http.HandleFunc("/js/app.js", viewAppJs)
	http.HandleFunc("/", viewStatic)
	log.Printf("Listening on port %d", port)
	authServer.httpServer = &http.Server{
		Addr:           fmt.Sprintf(":%d", port),
		Handler:        nil, // Use default mutex handler
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(authServer.httpServer.ListenAndServe())
}

func viewStatic(w http.ResponseWriter, r *http.Request) {
	log.Printf("Attempting to handle static resource at path '%s' from '%s'.", r.URL.Path, r.RemoteAddr)
	s := r.URL.Path[1:]
	if s == "" {
		s = "index.html"
	}
	path := "content/" + s
	log.Printf("Attempting to get resource from %s", path)
	content, err := Asset(path)
	if err != nil {
		log.Printf("Static resource at '%s' for path '%s' not found.", path, r.URL.Path)
	}
	ext := filepath.Ext(path)
	if ext != "" {
		ct := mime.TypeByExtension(ext)
		if ext != "" {
			w.Header().Add("content-type", ct)
		}
	}
	w.Write(content)
}

func viewAppJs(w http.ResponseWriter, r *http.Request) {
	log.Printf("Attempting to handle request for app.js at path '%s' from '%s'.", r.URL.Path, r.RemoteAddr)
	path := "content/js/app.js"
	content, err := Asset(path)
	if err != nil {
		log.Printf("Static resource at '%s' for path '%s' not found.", path, r.URL.Path)
	}
	template, err := template.New("app.js").Parse(string(content[:]))
	if err != nil {
		log.Fatalf(err.Error())
		os.Exit(1)
	}
	err = template.Execute(w, &authServer)
	if err != nil {
		log.Fatalf(err.Error())
		os.Exit(1)
	}
}

func reportToken(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get("token")
	t, err := jwt.Parse(token, signingKeyForToken)
	if err == nil && t.Valid {
		tokens, err := tokens.LoadTokens(authServer.tokenPath)
		if err != nil {
			log.Fatalf("Unable to load stored tokens: %s", err)
		}
		tokens[authServer.tokenKey] = token
		err = tokens.SaveTokens(authServer.tokenPath)
		if err != nil {
			log.Fatal("Unable to store token: %s", err)
		} else {
			w.WriteHeader(204)
			w.Write([]byte{}) // empty results
			if f, ok := w.(http.Flusher); ok {
				f.Flush()
			}
			fmt.Println(token)
			// give the response time to go to the browser
			time.Sleep(1000 * time.Millisecond)
			log.Printf("Sending token to channel")
			authServer.finished <- &token
		}
	}
}

var signingKeys = make(map[string]interface{})

func loadSigningTokens(profiles Profiles) error {
	for _, env := range profiles {
		keys, err := env.ObtainSigningKeys()
		if err != nil {
			return err
		}
		for kid, key := range keys {
			log.Printf("Loaded signing key with kid %s for environment %s, tenant %s", kid, env.Name, env.AuthConfig.Domain)
			signingKeys[kid] = key
		}
	}
	return nil
}

func signingKeyForToken(token *jwt.Token) (interface{}, error) {
	kid := token.Header["kid"]
	if kid == nil {
		return nil, fmt.Errorf("No kid in the header of the token")
	}
	id := fmt.Sprintf("%s", kid)
	key, ok := signingKeys[id]
	if ok {
		log.Printf("Found signing key for kid %s", id)
		return key, nil
	} else {
		return nil, fmt.Errorf("No signing key found for kid %s", id)
	}
}
