#!/bin/bash

# This build relies on go-bindata to generate the go-bindata.go file.
# go get -u github.com/jteeuwen/go-bindata

version=$(git tag --list | grep '^v' | sort | tail -1 | cut -c 2-)-DEBUG
echo "Building version $version"

echo "Generate the gobindata.go file containing static content"
go-bindata $(find content -type d)

export cur=$(pwd)
export PATH="$PATH:$cur"
go build -gcflags "all=-N -l"

echo "Starting the application with dlv for debugging, connect to localhost:2345"
dlv --listen=127.0.0.1:2345 --headless=true --api-version=2 exec ./authenticator -- "$@"
