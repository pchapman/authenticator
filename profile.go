package main

import (
	"fmt"
	"gitlab.com/pchapman/gooauth"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Profile struct {
	Name                string         `json:"name" yaml:"name"`
	ServiceAppId        string         `json:"serviceAppId" yaml:"serviceAppId"`
	AuthConfig          gooauth.Config `json:"authConfig" yaml:"authConfig"`
	accessTokenProvider gooauth.AccessTokenProvider
}

//func (profile Profile) GetAuthenticationProviderType() gooauth.AuthenticationProviderType {
//	if profile.accessTokenProvider == nil {
//		return ""
//	}
//	return profile.accessTokenProvider.GetAuthenticationProviderType()
//}

func (profile Profile) IsServerToServer() bool {
	//TODO:
	return true
}

func (profile Profile) IsUserAuthenticated() bool {
	//TODO:
	return false
}

func (profile Profile) ObtainAccessToken() (string, error) {
	if profile.accessTokenProvider == nil {
		// No provider
		return "", fmt.Errorf("No access token provider available for the profileironment %s", profile.Name)
	} else {
		token, err := gooauth.ObtainAccessToken(profile.accessTokenProvider, profile.ServiceAppId)
		return token, err
	}
}

func (profile Profile) ObtainSigningKeys() (map[string]interface{}, error) {
	if profile.accessTokenProvider == nil {
		keys := make(map[string]interface{})
		return keys, nil
	} else {
		keys, err := gooauth.ObtainSigningKeys(profile.accessTokenProvider)
		return keys, err
	}
}

type Profiles []Profile

func (envs *Profiles) Find(name string) *Profile {
	for _, env := range *envs {
		if env.Name == name {
			return &env
		}
	}
	return nil
}

func LoadProfiles(configFilePath string) (Profiles, error) {
	if "" == configFilePath {
		fmt.Errorf("Missing configuration path")
	}

	// Read configuration from config path.
	body, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, err
	}
	configs := make(Profiles, 0, 0)
	err = yaml.Unmarshal(body, &configs)
	if err != nil {
		return nil, err
	}
	for i, p := range configs {
		provider := gooauth.NewProvider(&p.AuthConfig)
		if provider == nil {
			log.Printf("Unable to load authentication provider for profile %s", p.Name)
		} else {
			p.accessTokenProvider = provider
			log.Printf("Loaded authentication provider for profile %s", p.Name)
			configs[i] = p
		}
	}
	log.Printf("Loaded configurations for %d profiles", len(configs))
	return configs, nil
}
