package tokens

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type StoredTokens map[string]string

func LoadTokens(path string) (StoredTokens, error) {
	tokens := make(StoredTokens)

	// Read configuration from config path.
	body, err := ioutil.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			// File does not exist.  Return empty slice
			return tokens, nil
		}
		return nil, err
	}

	// Parse file contents
	err = yaml.Unmarshal(body, tokens)
	if err != nil {
		return nil, err
	}
	return tokens, nil
}

func (tokens StoredTokens) SaveTokens(path string) error {
	body, err := yaml.Marshal(tokens)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(path, body, os.ModePerm)
	return err
}
