#!/bin/bash

# This build relies on go-bindata to generate the go-bindata.go file.
# go get -u github.com/jteeuwen/go-bindata

version=$(git tag --list | grep '^v' | sort | tail -1 | cut -c 2-)
echo "Building version $version"

echo "Generate the gobindata.go file containing static content"
go-bindata $(find content -type d)

echo "Building application"
go build -ldflags "-X main.version=$version"

echo "Done"
