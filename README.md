# authenticator
A command-line application for obtaining OAuth tokens which can be used to authenticate web service calls. This is
useful in shell scripts and other one-off uses for development/support purposes.

This app currently supports Auth0 (https://auth0.com) for machine-to-machine and user on-behalf-of tokens.  Other
providers may be added as need requires.  All tokens are cached for future use. A new token will only be requested from
the OAuth provider if none are cached for the profile or the cached token has expired.

For machine-to-machine tokens, it will simply print a valid token on the command line stdout.

For user on-behalf-of tokens, a browser window will be displayed which will redirect to the application's login page on
the OAuth provider's network.  Once the user has successfully logged in, the token will be printed to the command line
stdout.  In addition to this, the browser window will display the token and provide a means for copying the token to
clipboard.

Authenticator looks for cached tokens in a file called ".tokens.yaml" within a directory ".authenticator" inside the
user's home directory.

Authenticator allows you to select between "profiles" for obtaining an access token.  The information on how to obtain
the tokens are found, by default in a file called ".profiles.yaml" within a directory ".authenticator" within the user's
home directory.  Here is an example of a ".profiles.yaml" file with one machine-to-machine profile and one user profile:

```
# An example of a machine-to-machine profile. The name can be anything.
- name: MTM Profile
  tenantId: sample-tenant
  clientId: ABCDEFGHIJKLMNOPQRSTUVWXYZ123456
  clientSecret: "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz12"
  serviceAppId: "https://myservice.myapp.com"

# An example of a user profile.  The name can be anything.  Notice that there is no secret.  The client account must be
# configured in the OAuth provider to redirect to http://localhost:9999 after successful authentication (Not recommended
# for production systems).
- name: User Profile
  tenantId: db-ams-dev
  clientId: abcdefghijklmnopqrstuvwxyz123456
  clientSecret: ""
  serviceAppId: "https://myservice.myapp.com"
```

**Note that because machine-to-machine profiles contain both client ID and client secret, access to this file should be
carefully protected and should never contain production information**


## Usage
```
Usage of authenticator:
  -c string
    	The configuration file to load environments from.
  -d	Debug the application, showing additional info.
  -p string
    	The profile to authenticate against.
  -v	Print version info and exit.
```