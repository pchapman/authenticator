window.addEventListener('load', function() {

  document.getElementById("copyAccessToken").addEventListener('click', function() {
    copyText = document.getElementById("accessToken")
    copyText.select();

    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
    } catch (err) {
      console.log('Oops, unable to copy');
    }
  });

  var webAuth = new auth0.WebAuth({
    domain: "{{.TenantId}}.auth0.com",
    clientID: "{{.ClientId}}",
    redirectUri: "http://localhost:9999/",
    audience: "{{.ServiceAppId}}",
    responseType: 'token id_token',
    scope: 'openid profile read:messages',
    leeway: 60
  });

  function logout() {
    var accessToken = localStorage.removeItem('access_token');
    document.location = document.location
  }

  function report() {
    var accessToken = localStorage.getItem('access_token');
    var url = "report?token=" + accessToken;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    console.log("Reporting access token");
    xhr.onload = function() {
      console.log("Got response " + xhr.status + " back from endpoint");
      if (xhr.status  == 200 || xhr.status == 204) {
        // update message
        document.getElementById('accessToken').innerText=accessToken;
        document.getElementById("msg").innerText="You are logged in and may close this window.";
      } else {
        console.log('Request failed: ' + xhr.statusText);
      }
    };
    xhr.send();
  }

  function setSession(authResult) {
    // Set the time that the access token will expire at
    var expiresAt = JSON.stringify(
        authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  function isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    if (expiresAt) {
      return new Date().getTime() < expiresAt;
    } else {
      return false;
    }
  }

  function handleAuthentication() {
    webAuth.parseHash(function(err, authResult) {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        setSession(authResult);
      } else if (err) {
        console.log(err);
        document.getElementById("msg").innerHTML='Error: ' + err.error + '. Check the console for further details.';
      }
      if (isAuthenticated()) {
        report()
      } else {
        webAuth.authorize();
      }
    });
  }

  handleAuthentication();
});
